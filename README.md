## AIDL接口定义

## 功能:通过aidl调用实现 pdf生成,pdf预览


### 一、服务端apk安装后在安卓上不出现图标并且实现自动启动


### 二、客户端调用

#### 1.链接服务端

```
  var pdfAidlInterface: SignPdfAidlInterface? = null
    val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
     
            pdfAidlInterface = SignPdfAidlInterface.Stub.asInterface(iBinder)


        }

        override fun onServiceDisconnected(componentName: ComponentName) {
     
        }
    }
    
val intent = Intent()
intent.component = ComponentName(
"com.zchd.sign.pdf.service", "com.zchd.sign.pdf.service.SignPdfService"
)
bindService(intent, serviceConnection, BIND_AUTO_CREATE)
```

#### 2.调用服务端接口生成pdf

 ```
        val signPdfInfo = SignPdfInfo(
                userName = "王天一",
                userCardType = "身份证",
                userIdCard = "524000000000000",
                houseRelation = "/",
                repName = "测试",
                repCardType = "身份证",
                repIdCard = "123456789",
                authRelation = "/",
                signPicPath =null,
                fingerPicPath = null,
                repSignPicPath = null,
                repFingerPicPath = null

            )
            val sdCardPath =   Environment.getExternalStorageDirectory().absolutePath
            val pdfPath = "${sdCardPath}/sign_pdf.pdf"
      pdfAidlInterface?.sign(signPdfInfo, pdfPath, object : SignPdfCallback.Stub() {
                override fun onSuccess(path: String?) {
                
                //预览pdf
                    val intent = Intent(baseContext, WebViewActivity::class.java)
                    intent.putExtra("path", path)
                    startActivity(intent)
        
                }

                override fun onFail() {
               
                }
            })
```
 


