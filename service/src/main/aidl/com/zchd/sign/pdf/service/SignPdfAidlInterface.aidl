package com.zchd.sign.pdf.service;
import com.zchd.sign.pdf.service.SignPdfCallback;
parcelable SignPdfInfo;

interface SignPdfAidlInterface {
 void sign(in SignPdfInfo info,in String savePath, in SignPdfCallback callback);
}