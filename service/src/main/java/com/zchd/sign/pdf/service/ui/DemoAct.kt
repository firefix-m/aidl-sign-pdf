package com.zchd.sign.pdf.service.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Button
import com.blankj.utilcode.util.PermissionUtils
import com.zchd.sign.pdf.service.R


/**
 * Copyright (C), 2021-2023, 中传互动（湖北）信息技术有限公司
 * Author: 彭艳明
 * Date:2023年12月14日
 * Description:
 */


class DemoAct : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!PermissionUtils.isGranted(*PermissionActivity.permissions.toTypedArray())) {
            startActivity(
                Intent(this, PermissionActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            )
            return
        }
        val directory: String =
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)?.absolutePath
                ?: Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath
        Log.e(">>>>>>>>>","${directory}");
        findViewById<Button>(R.id.btn_create).setOnClickListener {
            startActivity(Intent(this, SignPadActivity::class.java))

        }

    }


}