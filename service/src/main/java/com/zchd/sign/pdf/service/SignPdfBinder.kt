package com.zchd.sign.pdf.service

import android.content.Context
import android.content.Intent
import android.os.Environment
import android.util.Log
import com.blankj.utilcode.util.PermissionUtils
import com.zchd.sign.pdf.service.ui.PermissionActivity
import com.zchd.sign.pdf.service.utils.PdfComposeToWrite
import com.zchd.sign.pdf.service.SignPdfAidlInterface

/**
 * Copyright (C), 2021-2023, 中传互动（湖北）信息技术有限公司
 * Author: 彭艳明
 * Date:2023年11月22日
 * Description:
 */
class SignPdfBinder(private val context: Context) : SignPdfAidlInterface.Stub() {

    override fun sign(info: SignPdfInfo?, savePath: String?, callback: SignPdfCallback?) {

        if (!PermissionUtils.isGranted(*PermissionActivity.permissions.toTypedArray())) {
            context.startActivity(
                Intent(
                    context,
                    PermissionActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        } else {
            if (savePath.isNullOrBlank()) {
                callback?.onFail()
                return
            }

        }
        if (savePath.isNullOrBlank()) {
            callback?.onFail()
            return
        }
        PdfComposeToWrite().create(savePath).buildAuthorizeBook(info) { it ->
            callback?.onSuccess(it)
        }
        return
        Log.e(">>>>>>>>>>>>>>>>", "sign")
    }

}