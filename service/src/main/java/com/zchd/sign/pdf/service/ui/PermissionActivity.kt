package com.zchd.sign.pdf.service.ui

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import com.blankj.utilcode.util.PermissionUtils
import com.blankj.utilcode.util.PermissionUtils.SimpleCallback

class PermissionActivity : Activity() {
    companion object {
        val permissions = arrayListOf<String>().apply {
            add(READ_EXTERNAL_STORAGE)
            add(WRITE_EXTERNAL_STORAGE)
        }
    }

    private val REQUEST_CODE = 1024
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PermissionUtils.permission(*permissions.toTypedArray()).callback(object : SimpleCallback {
            override fun onGranted() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    if (!Environment.isExternalStorageManager()) {
                        val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                        intent.data = Uri.parse("package:" + application.packageName)
                        startActivityForResult(intent, REQUEST_CODE)
                    } else {
                        finish()
                    }
                    return
                }
                finish()
            }

            override fun onDenied() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    if (!Environment.isExternalStorageManager()) {
                        val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                        intent.data = Uri.parse("package:" + application.packageName)
                        startActivityForResult(intent, REQUEST_CODE)
                    } else {
                        finish()
                    }
                    return
                }
                finish()
            }
        }).request()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (!Environment.isExternalStorageManager()) {
                    finish()
                } else {
                    finish()
                }
            }
        }
    }
}