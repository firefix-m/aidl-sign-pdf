package com.zchd.sign.pdf.service.utils

import com.itextpdf.io.font.PdfEncodings
import com.itextpdf.io.font.constants.StandardFonts
import com.itextpdf.io.image.ImageDataFactory
import com.itextpdf.kernel.colors.ColorConstants
import com.itextpdf.kernel.font.PdfFontFactory
import com.itextpdf.kernel.geom.PageSize
import com.itextpdf.kernel.pdf.EncryptionConstants
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.kernel.pdf.WriterProperties
import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Cell
import com.itextpdf.layout.element.Image
import com.itextpdf.layout.element.Paragraph
import com.itextpdf.layout.element.Table
import com.itextpdf.layout.property.TextAlignment
import com.itextpdf.layout.property.UnitValue
import com.itextpdf.layout.property.VerticalAlignment
import com.zchd.sign.pdf.service.SignPdfInfo
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.Date


class PdfComposeToWrite {
    private var savePdfPath: String? = null
    private val fontStyle = Utils.getAssetsCacheFile(Utils.getApplication(), "SimSun.ttf")
    private val font = PdfFontFactory.createFont(fontStyle, PdfEncodings.IDENTITY_H, true)

    private var pdfDocument: Document? = null
    private var mSignInfo: SignPdfInfo? = null
    fun create(pdfPath: String): PdfComposeToWrite {
        this.savePdfPath = pdfPath
        try {
//            pdfDocument?.close()
            val wp = WriterProperties()
            wp.setStandardEncryption(
                null,
                null,
                EncryptionConstants.ALLOW_PRINTING,
                EncryptionConstants.ENCRYPTION_AES_128
            )
            var pdfWriter = PdfWriter(FileOutputStream(pdfPath), wp)
            val document = PdfDocument(pdfWriter)
            pdfDocument = Document(document, PageSize.A4, false).apply {
                setFont(PdfFontFactory.createFont(StandardFonts.COURIER))
                setMargins(60.0f, 110f, 0.0f, 110f)
                setFontSize(10f)
                setFontColor(ColorConstants.BLACK)
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return this
    }

    fun buildAuthorizeBook(signInfo: SignPdfInfo? = null, callback: (String) -> Unit) {
        mSignInfo = signInfo
        pdfDocument?.add(Paragraph(title).apply {
            setTextAlignment(TextAlignment.CENTER)
            setFont(font)
            setBold()
            setFontSize(12f)
            setMarginBottom(20f)
        })
        content.forEach {
            pdfDocument?.add(Paragraph(it).apply {
                setFont(font)
                setFirstLineIndent(20f)
            })
        }
        pdfDocument?.add(Paragraph(
            "本人声明:本人已仔细阅读上述所有条款及填写须知，且对所有条款的含义及相应的法律后果已全部知晓并充分理解,本人自愿作出上述授权、承诺和声明。"
        ).apply {
            setFont(font)
            setBold()
            setFontSize(12f)
            setFirstLineIndent(20f)
        })
        createTable1()
        createTable2()
        pdfDocument?.add(Paragraph(
            "经办人员:"
        ).apply {
            setFont(font)
            setMarginTop(10f)
            setBold()
        })
        pdfDocument?.add(Paragraph(
            DateUtil.convertToString(Date().time, DateUtil.FORMAT_YYYYCMMCDD)
        ).apply {
            setFont(font)
            setFontSize(10f)
            setMarginTop(10f)
            setTextAlignment(TextAlignment.RIGHT)
        })
        pdfDocument?.add(Paragraph(
            "填写须知："
        ).apply {
            setFont(font)
            setBold()
            setMarginTop(20f)
            setFontColor(ColorConstants.BLACK)
        })
        pdfDocument?.add(Paragraph(
            remark
        ).apply {
            setFont(font)
            setMarginTop(10f)
        })
        try {
            pdfDocument?.close()
        } catch (e: Exception) {
        }
        callback.invoke(savePdfPath ?: "")

    }

    private fun createTable1() {
        pdfDocument?.add(Paragraph("授权人信息").apply {
            setTextAlignment(TextAlignment.CENTER)
            setFont(font)
            setBold()
            setMarginTop(16f)
        })
        val unitValue = arrayOf(
            UnitValue.createPercentValue(10f),
            UnitValue.createPercentValue(15f),
            UnitValue.createPercentValue(22f),
            UnitValue.createPercentValue(15f),
            UnitValue.createPercentValue(15f),
            UnitValue.createPercentValue(20f)
        )
        val table = Table(unitValue).apply {
            width = UnitValue.createPercentValue(100f)
            setFont(font)
            setFixedLayout()
            setFontSize(10f)
            setVerticalAlignment(VerticalAlignment.MIDDLE)
            setTextAlignment(TextAlignment.CENTER)
        }
        table.addHeaderCell("姓名")
        table.addHeaderCell("证件类型")
        table.addHeaderCell("证件号")
        table.addHeaderCell("与户主关系")
        table.addHeaderCell("签名/指印")
        table.addHeaderCell("委托代理人/法\n定代理人姓名")
        table.addFooterCell(Cell().apply {
            setHeight(20f)
            add(Paragraph(mSignInfo?.userName ?: ""))
        })
        table.addFooterCell(mSignInfo?.userCardType ?: "")
        table.addFooterCell(mSignInfo?.userIdCard ?: "")
        table.addFooterCell(mSignInfo?.houseRelation ?: "")
        val tableWidth = table.width
        table.addFooterCell(Cell().apply {
            if (mSignInfo?.fingerPicPath.isNullOrEmpty() && mSignInfo?.signPicPath.isNullOrEmpty()) {
                if (null != mSignInfo) {
                    add(Paragraph("/"))
                }
            } else {
                if (mSignInfo?.signPicPath?.isNotBlank() == true) {
                    add(Image(ImageDataFactory.create(mSignInfo?.signPicPath)).apply {
                        width = UnitValue.createPercentValue(80f)
                    })

                }
                if (mSignInfo?.fingerPicPath?.isNotBlank() == true) {
                    add(Image(ImageDataFactory.create(mSignInfo?.fingerPicPath)).apply {
                        width = UnitValue.createPercentValue(80f)
//                        setFixedPosition(1,350f,230f,80f)
                    })

                }

            }
        })
//        table.addFooterCell(Image(ImageDataFactory.create(mSignInfo?.signPicPath)).apply {
//            width = UnitValue.createPercentValue(80f)
//        })
        table.addFooterCell(mSignInfo?.repName ?: "")

        pdfDocument?.add(table)
    }

    private fun createTable2() {
        pdfDocument?.add(Paragraph("委托代理人/法定代理人信息").apply {
            setTextAlignment(TextAlignment.CENTER)
            setFont(font)
            setBold()
            setMarginTop(8f)
        })
        val unitValue = arrayOf(
            UnitValue.createPercentValue(10f),
            UnitValue.createPercentValue(15f),
            UnitValue.createPercentValue(22f),
            UnitValue.createPercentValue(15f),
            UnitValue.createPercentValue(15f),
        )
        val table = Table(unitValue).apply {
            width = UnitValue.createPercentValue(100f)
            setFont(font)
            setFixedLayout()
            setFontSize(10f)
            setTextAlignment(TextAlignment.CENTER)
        }
        table.addHeaderCell("姓名")
        table.addHeaderCell("证件类型")
        table.addHeaderCell("证件号")
        table.addHeaderCell("与授权人关系")
        table.addHeaderCell("签名/指印")
        table.addFooterCell(Cell().apply {
            setHeight(20f)
            add(Paragraph(mSignInfo?.repName ?: ""))
        })
        table.addFooterCell(mSignInfo?.repCardType ?: "")
        table.addFooterCell(mSignInfo?.repIdCard ?: "")
        table.addFooterCell(mSignInfo?.authRelation ?: "").apply {}
        table.addFooterCell(Cell().apply {
            if (mSignInfo?.repSignPicPath.isNullOrEmpty() && mSignInfo?.repSignPicPath.isNullOrEmpty()) {
                if (null != mSignInfo) {
                    add(Paragraph("/"))
                }
            } else {
                if (mSignInfo?.repSignPicPath?.isNotBlank() == true) {
                    add(Image(ImageDataFactory.create(mSignInfo?.repSignPicPath)).apply {
                        width = UnitValue.createPercentValue(80f)
                    })
                }
                if (mSignInfo?.repFingerPicPath?.isNotBlank() == true) {
                    add(Image(ImageDataFactory.create(mSignInfo?.repFingerPicPath)).apply {
                        width = UnitValue.createPercentValue(80f)
                    })
                }

            }

        })
        pdfDocument?.add(table)
    }

    private var title = "经济状况核对授权书"
    private var content = arrayListOf<String>(
        "本人同意授权审批机构及全国各级居民家庭经济状况核对机构通 过政府机构、金融机构、提供货币资金转移服务的非银行支付机构、 大数据管理及服务机构、公共事业单位、相关行业性组织和社会团体等 涉及本人基本信息及家庭经济状况信息的机构、单位、部门，就社会救 助、社会保障、社会福利以及其他需要依据居民家庭经济状况进行行政 确认、行政给付、行政审批的相关事项，对本人基本信息及家庭经济状况信息进行查询、 核对 。",
        "本人亦同意授权合法留存本人基本信息和家庭经济状况信息的前述机构予以配合提供本人基本信息和家庭经济状况信息。",
        "本授权有效期限自签署之日起至申请人5 退出该行政事项止，含申请人纳入监测范围期间。",
        "本人承诺以下身份证件号码、签名（或指印）均真实有效，如有虚构、隐瞒、伪造，本人愿意承担相应法律责任及后果。"
    )
    private var remark =
        "1、审批机构1包括但不限于：乡镇（街道）及以上人民政府、县级及以上社会救助、社会福利等社会保障类主管部门、乡村振兴部门。\n2、司法机关2包括但不限于：法院、检察院。\n3、政府机构3包括但不限于：发展改革、教育、公安、司法行政、财政、税务、民政、人力资源社会保障、医保、规划和自然资源、住房城乡建设、海事、退役军人、农业农村、乡村振兴、卫生健康、市场监管、金融监管、应急管理、通信管理、能源、统计、政务数据管理，以及法律、法规授权的具有管理公共事务职能的组织。\n4、群团组织4包括但不限于：残联、工会。\n5、申请人5指授权人本人以及与本人相关的其他申请人。\n6、授权人为无民事行为能力人、限制民事行为能力人的，由其法定代理人签署，并在委托代理人/法定代理人信息表中填写相关信息。\n7、采用纸质授权书方式授权的，应由授权人本人或代理人亲笔签名或按捺指印以确认;采用电子授权书方式授权的,需经信息比对以确认授权人本人或代理人身份后,通过可靠的电子签名方式确认授权。\n8、委托代理人应确保其于签署本授权书时已取得授权人本人的有效授权，并就授权的真实性和合法性承担相应法律责任。"

}