package com.zchd.sign.pdf.service.widget

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.Nullable
import androidx.lifecycle.MutableLiveData
import com.zchd.sign.pdf.service.utils.Utils
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class PaintView : View {
    private var mPaint: Paint? = null
    private var cacheCanvas: Canvas? = null
    private var mPath: Path? = null
    private var mCacheBitmap: Bitmap? = null
    var hasSignLiveData = MutableLiveData<Boolean>(false)

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    private fun init() {
        mPaint = Paint()
        mPaint?.isAntiAlias = true
        mPaint?.strokeWidth = 20.0f
        mPaint?.style = Paint.Style.STROKE
        mPaint?.color = Color.BLACK
        mPaint?.isDither = true
        mPaint?.isFilterBitmap = true
        mPaint?.typeface = Typeface.DEFAULT_BOLD
        mPaint?.isFakeBoldText = true
        mPaint?.strokeCap = Paint.Cap.ROUND
        mPaint?.strokeJoin = Paint.Join.MITER
        mPath = Path()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (measuredWidth <= 0 || measuredHeight <= 0) {
            return
        }
        mCacheBitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
        mCacheBitmap?.let {
            cacheCanvas = Canvas(it)
        }
    }

    override fun onDraw(canvas: Canvas) {
        mCacheBitmap?.let {
            canvas.drawBitmap(it, 0f, 0f, null)
        }
    }

    private var mLastTouchX = 0f
    private var mLastTouchY = 0f
    override fun onTouchEvent(event: MotionEvent): Boolean {
        hasSignLiveData.value = true
        val eventX = event.x
        val eventY = event.y
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                mPath?.moveTo(eventX, eventY)
                mLastTouchX = eventX
                mLastTouchY = eventY
            }

            MotionEvent.ACTION_MOVE -> {
                mPath?.quadTo(mLastTouchX, mLastTouchY, eventX, eventY)
                cacheCanvas?.drawPath(mPath!!, mPaint!!)
                invalidate()
                mLastTouchX = eventX
                mLastTouchY = eventY
            }

            MotionEvent.ACTION_UP -> {
                mPath?.reset()
            }
        }
        return true
    }

    fun saveSignFile(): File {
        var file = File(Utils.getApplication().cacheDir.path + "/sign.png")
        try {
            var whiteBitmap = convertTransparentToWhite(mCacheBitmap!!)
            val width = whiteBitmap!!.width * 0.5
            val height = whiteBitmap!!.height * 0.5
            val scaledBitmap =
                Bitmap.createScaledBitmap(whiteBitmap!!, width.toInt(), height.toInt(), true)
            var bos = BufferedOutputStream(FileOutputStream(file))
            scaledBitmap?.compress(Bitmap.CompressFormat.PNG, 100, bos)
            bos.flush()
            bos.close()
            scaledBitmap.recycle()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    private fun convertTransparentToWhite(originalBitmap: Bitmap): Bitmap? {
        val width = originalBitmap.width
        val height = originalBitmap.height
        val whiteBackgroundBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(whiteBackgroundBitmap)
        val paint = Paint()
        paint.color = Color.WHITE
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
        canvas.drawBitmap(originalBitmap, 0f, 0f, paint)
        return whiteBackgroundBitmap
    }

    fun getBitmap(): Bitmap {
        return mCacheBitmap!!
    }

    fun clear() {
        hasSignLiveData.value = false
        cacheCanvas?.drawColor(0, PorterDuff.Mode.CLEAR)
        invalidate()
    }
}
