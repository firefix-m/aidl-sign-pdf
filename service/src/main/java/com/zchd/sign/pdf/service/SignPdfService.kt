package com.zchd.sign.pdf.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.blankj.utilcode.util.Utils
import com.zchd.sign.pdf.service.utils.PdfComposeToWrite

/**
 * Copyright (C), 2021-2023, 中传互动（湖北）信息技术有限公司
 * Author: 彭艳明
 * Date:2023年11月22日
 * Description:
 */
class SignPdfService : Service() {
    override fun onBind(intent: Intent?): IBinder = SignPdfBinder(applicationContext)
    override fun onCreate() {
        super.onCreate()
        Utils.init(application)
    }
}