package com.zchd.sign.pdf.service.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.blankj.utilcode.util.JsonUtils
import com.zchd.sign.pdf.service.R
import com.zchd.sign.pdf.service.SignPdfInfo
import com.zchd.sign.pdf.service.utils.PdfComposeToWrite
import com.zchd.sign.pdf.service.utils.Utils
import com.zchd.sign.pdf.service.widget.PaintView
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File


/**
 * Copyright (C), 2021-2023, 中传互动（湖北）信息技术有限公司
 * Author: 彭艳明
 * Date:2024年01月10日
 * Description:
 */
class SignPadActivity : Activity() {
    private var name = "张三丰"
    private var idCard = "420384445454564611"
    private lateinit var signFile: File
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_pad)
        val signView = findViewById<PaintView>(R.id.signView)
        findViewById<TextView>(R.id.tv_title_remind).text =
            Utils.getSpan(name, "请 ${name} 按照要求规范签署您的姓名")
        findViewById<TextView>(R.id.tv_sign_remind).text = name
        findViewById<TextView>(R.id.btn_cancel).setOnClickListener {
            signView.clear()
        }
        findViewById<TextView>(R.id.btn_confirm).setOnClickListener {
            if (signView.hasSignLiveData.value == false) {
                Toast.makeText(this, "请完成签字", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            signFile = signView.saveSignFile()
            ocrImageToText(signFile)
        }
    }

    private fun ocrImageToText(file: File) {
        val client = OkHttpClient()
        val requestBody: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
            .addFormDataPart("image", file.name, file.asRequestBody()).build()
        var request =
            Request.Builder().url("http://192.168.70.85:8084/ocr").post(requestBody).build();
        Thread(Runnable {
            val responseBody = client.newCall(request).execute().body
            if (responseBody != null) {
                val string = responseBody.string()
                val code = JsonUtils.getInt(string, "code")
                if (code == 0) {
                    val data = JsonUtils.getString(string, "data")
                    if (checkEquals(data)) {
                        createPdfBook()
                    } else {
                        runOnUiThread(Runnable {
                            Toast.makeText(this, "签名错误请重新签字", Toast.LENGTH_LONG).show()
                        })
                    }
                }

            }
        }).start()

    }

    private fun checkEquals(text: String): Boolean {
        return text == name
    }

    private fun createPdfBook() {
        val signPdfInfo = SignPdfInfo(
            userName = "张三丰",
            userCardType = "身份证",
            userIdCard = "524000000000000",
            houseRelation = "/",
            repName = "测试",
            repCardType = "身份证",
            repIdCard = "520145666522555459",
            authRelation = "/",
            signPicPath = signFile.absolutePath,
            fingerPicPath = null,
            repSignPicPath = null,
            repFingerPicPath = null

        )
        val sdCardPath = Environment.getExternalStorageDirectory().absolutePath
        val pdfPath = "${sdCardPath}/your_pdf.pdf"
        PdfComposeToWrite().create(pdfPath).buildAuthorizeBook(signPdfInfo) { it ->
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra("path", it)
            startActivity(intent)
        }
    }

}