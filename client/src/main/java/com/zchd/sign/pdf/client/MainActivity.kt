package com.zchd.sign.pdf.client

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Environment
import android.os.IBinder
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.PermissionUtils
import com.zchd.sign.pdf.service.SignPdfAidlInterface
import com.zchd.sign.pdf.service.SignPdfCallback
import com.zchd.sign.pdf.service.SignPdfInfo

class MainActivity : AppCompatActivity() {
    private var pdfAidlInterface: SignPdfAidlInterface? = null
    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            pdfAidlInterface = SignPdfAidlInterface.Stub.asInterface(iBinder)
            Toast.makeText(this@MainActivity, "链接成功", Toast.LENGTH_LONG).show()

        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Toast.makeText(this@MainActivity, "链接失败", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!PermissionUtils.isGranted(*PermissionActivity.permissions.toTypedArray())) {
            startActivity(
                Intent(this, PermissionActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        }
        findViewById<Button>(R.id.btn_connect).setOnClickListener {
            val intent = Intent()
            intent.component = ComponentName("com.zchd.sign.pdf.service", "com.zchd.sign.pdf.service.SignPdfService"
            )
            bindService(intent, serviceConnection, BIND_AUTO_CREATE)
        }
        findViewById<Button>(R.id.btn_preview).setOnClickListener {
            val signPdfInfo = SignPdfInfo(
                userName = "王天一",
                userCardType = "身份证",
                userIdCard = "524000000000000",
                houseRelation = "/",
                repName = "测试",
                repCardType = "身份证",
                repIdCard = "520145666522555459",
                authRelation = "/",
                signPicPath = null,
                fingerPicPath = null,
                repSignPicPath = null,
                repFingerPicPath = null

            )
            val sdCardPath = Environment.getExternalStorageDirectory().absolutePath
            val pdfPath = "${sdCardPath}/sign_pdf.pdf"
            pdfAidlInterface?.sign(signPdfInfo, pdfPath, object : SignPdfCallback.Stub() {
                override fun onSuccess(path: String?) {
                    Toast.makeText(this@MainActivity, "服务端生成pdf成功", Toast.LENGTH_LONG).show()
                    val intent = Intent(baseContext, WebViewActivity::class.java)
                    intent.putExtra("path", path)
                    startActivity(intent)
                }

                override fun onFail() {
                    Toast.makeText(this@MainActivity, "服务端生成pdf失败", Toast.LENGTH_LONG).show()
                }
            })
        }
    }
}