package com.zchd.sign.pdf.service

import android.os.Parcel
import android.os.Parcelable

data class SignPdfInfo(
    val userName: String?,
    val userCardType: String?,
    val userIdCard: String?,
    val houseRelation: String?,
    val signPicPath: String?,
    val fingerPicPath: String?,
    val repName: String?,
    val repCardType: String?,
    val repIdCard: String?,
    val authRelation: String?,
    val repSignPicPath: String?,
    val repFingerPicPath: String?,
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userName)
        parcel.writeString(userCardType)
        parcel.writeString(userIdCard)
        parcel.writeString(houseRelation)
        parcel.writeString(signPicPath)
        parcel.writeString(fingerPicPath)
        parcel.writeString(repName)
        parcel.writeString(repCardType)
        parcel.writeString(repIdCard)
        parcel.writeString(authRelation)
        parcel.writeString(repSignPicPath)
        parcel.writeString(repFingerPicPath)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SignPdfInfo> {
        override fun createFromParcel(parcel: Parcel): SignPdfInfo {
            return SignPdfInfo(parcel)
        }

        override fun newArray(size: Int): Array<SignPdfInfo?> {
            return arrayOfNulls(size)
        }
    }


}