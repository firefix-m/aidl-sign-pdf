// SignPdfCallback.aidl
package com.zchd.sign.pdf.service;

interface SignPdfCallback {
   void onSuccess(String path);
   void onFail();
}